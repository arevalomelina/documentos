USE [Cedesistemas]
GO

/****** Object:  StoredProcedure [security].[ObtenerMenu]    Script Date: 2/10/2018 16:17:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Melina Arevalo
-- Create date: 2018-09-29
-- Description:	Obtener menu de acuerdo al rol
-- EJEMPLO: EXEC security.ObtenerMenu 'marevalo'
-- =============================================
CREATE PROCEDURE [security].[ObtenerMenu] 
	-- Add the parameters for the stored procedure here
	@ps_username varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	DECLARE @Rol int

	SELECT	@Rol = RolId 
	from	[security].[Permisos] P 
			inner join [security].[Usuarios] U ON P.UsuarioId = U.UsuarioId
	WHERE U.Username = @ps_username


	SELECT A.[AsegurableId]
		  ,[Nombre]
		  ,[Ruta]
		  ,[ParentId]
		  ,[TipoAsegurableId]
	  FROM [security].[Asegurables] A
			inner JOIN [security].[ControlAcceso] C
			ON A.AsegurableId = C.AsegurableId
	WHERE  C.RolId = @Rol 


END

GO


