USE [Cedesistemas]
GO

/****** Object:  StoredProcedure [security].[spAutenticarUsuario]    Script Date: 2/10/2018 16:18:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Melina Arevalo
-- Create date: 2018-09-22
-- Description:	Permite autenticar un usuario. Si el usuario es v�lido retorna el usuario id
-- Example:		exec security.spAutenticarUsuario 'marevalo', '8CB2237D0679CA88DB6464EAC60DA96345513964'
-- =============================================
CREATE PROCEDURE [security].[spAutenticarUsuario] 
	-- Add the parameters for the stored procedure here
	@ps_username varchar(50) 
	,@ps_password varchar (50)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here

	SELECT UsuarioId from security.Usuarios where [Username]= @ps_username and [Password] = @ps_password

END

GO


