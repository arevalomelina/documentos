USE [Cedesistemas]
GO

/****** Object:  StoredProcedure [security].[CrearUsuario]    Script Date: 2/10/2018 16:18:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		melina
-- Create date: 2018-09-29
-- Description:	Crear un nuevo usuario

-- =============================================
CREATE PROCEDURE [security].[CrearUsuario]
	-- Add the parameters for the stored procedure here
	@psUsername varchar(50)
	,@psPassword varchar(50)
	,@piRolId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @usuarioid int
INSERT INTO [security].[Usuarios]
           ([Username]
           ,[Password]
           ,[FechaCreacion]
           ,[FechaActualizacion]
           ,[FechaInactivacion]
           ,[Estado])
     VALUES
           (@psUsername
           ,@psPassword
           ,GETDATE()
           ,null
           ,null
           ,1)
	set @usuarioid = @@IDENTITY

	INSERT INTO [security].[Permisos]
           ([RolId]
           ,[UsuarioId])
     VALUES
           (@piRolId
           ,@usuarioid)

END

GO


