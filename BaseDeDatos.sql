use Cedesistemas;
create table security.Usuarios (
UsuarioId int not null IDENTITY(1,1) PRIMARY KEY
,Username varchar(50) not null
,Password varchar(50) not null
,FechaCreacion datetime not null
,FechaActualizacion datetime null
,FechaInactivacion datetime null
,Estado bit not null DEFAULT 0)

create table security.Roles (
RolId int not null IDENTITY(1,1) PRIMARY KEY
,Descrpcion varchar(50) not null
)

create table security.Permisos(
PermisoId int not null IDENTITY(1,1) PRIMARY KEY
,RolId int FOREIGN KEY REFERENCES security.Roles(RolId)
,UsuarioId int FOREIGN KEY REFERENCES security.Usuarios(UsuarioId)
)

create table security.TipoAsegurable (
TipoAsegurableId int not null IDENTITY(1,1) PRIMARY KEY
,Descrpcion varchar(50) not null
)

create table security.Asegurables(
AsegurableId int not null IDENTITY(1,1) PRIMARY KEY
,Nombre varchar(50) not null
,Ruta varchar(50) not null
,ParentId int FOREIGN KEY REFERENCES security.Asegurables(AsegurableId)
,TipoAsegurableId int FOREIGN KEY REFERENCES security.TipoAsegurable(TipoAsegurableId)
)
create table security.TipoPermiso (
TipoPermisoId int not null IDENTITY(1,1) PRIMARY KEY
,Descrpcion varchar(50) not null
)

create table security.ControlAcceso(
ControlAccesoId int not null IDENTITY(1,1) PRIMARY KEY
,RolId int FOREIGN KEY REFERENCES security.Roles(RolId)
,TipoPermisoId int FOREIGN KEY REFERENCES security.TipoPermiso(TipoPermisoId)
,AsegurableId int FOREIGN KEY REFERENCES security.Asegurables(AsegurableId)
)
