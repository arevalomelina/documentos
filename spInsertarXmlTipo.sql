-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Melina Arevalo
-- Create date: 20 de octubre de 2018
-- Description:	Insertar en batch usando xml
-- =============================================
CREATE PROCEDURE dbo.InsertarXmlTipo 
	-- Add the parameters for the stored procedure here
	@XmlTipo xml
AS
BEGIN
	SET NOCOUNT ON;

	insert into dbo.Tipos (TipoId,Descrpcion)
	SELECT  
	cast(tab.col.value('TipoId[1]','VARCHAR(max)')as int)AS TipoId,
	tab.col.value('Descrpcion[1]','VARCHAR(max)')AS Descrpcion
	FROM @XmlTipo.nodes('//Tipo') tab(col)END
GO
