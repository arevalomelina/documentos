use Cedesistemas;

DELETE FROM  [security].[Asegurables]
DELETE FROM [security].[TipoAsegurable]

DBCC CHECKIDENT ('[security].[Asegurables]', RESEED, 0)
DBCC CHECKIDENT ('[security].[TipoAsegurable]', RESEED, 0)

Declare @TipoAsegurable int 
,@ParentArchivo int
,@ParentReporte int
,@Rol1 int
,@Rol2 int
,@AsegurableId int
--Tipos de asegurables
INSERT INTO [security].[TipoAsegurable]
           ([Descrpcion])
     VALUES
           ('Menu')
INSERT INTO [security].[TipoPermiso]
           ([Descrpcion])
     VALUES
           ('Borrar')

set @TipoAsegurable=@@IDENTITY

-- Roles

INSERT INTO [security].[Roles]
           ([Descrpcion])
     VALUES
           ('Administrador')

set @Rol1 = @@Identity

INSERT INTO [security].[Roles]
           ([Descrpcion])
     VALUES
           ('Generico')

set @Rol2 = @@Identity

INSERT INTO [security].[Asegurables]
           ([Nombre]
           ,[Ruta]
           ,[ParentId]
           ,[TipoAsegurableId])
     VALUES
           ('Archivo'
           ,''
           ,null
           ,@TipoAsegurable)

set @ParentArchivo = @@IDENTITY

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol1
           ,1
           ,@ParentArchivo)

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol2
           ,1
           ,@ParentArchivo)

INSERT INTO [security].[Asegurables]
           ([Nombre]
           ,[Ruta]
           ,[ParentId]
           ,[TipoAsegurableId])
     VALUES
           ('Nuevo Usuario'
           ,'Proyecto.AplicacionEscritorio.Vistas.frmCrearUsuarios'
           ,@ParentArchivo
           ,@TipoAsegurable)

set @AsegurableId = @@IDENTITY

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol1
           ,1
           ,@AsegurableId)



INSERT INTO [security].[Asegurables]
           ([Nombre]
           ,[Ruta]
           ,[ParentId]
           ,[TipoAsegurableId])
     VALUES
           ('Salir'
           ,''
           ,@ParentArchivo
           ,@TipoAsegurable)

set @AsegurableId = @@IDENTITY

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol1
           ,1
           ,@AsegurableId)

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol2
           ,1
           ,@AsegurableId)

-------------------------
INSERT INTO [security].[Asegurables]
           ([Nombre]
           ,[Ruta]
           ,[ParentId]
           ,[TipoAsegurableId])
     VALUES
           ('Reportes'
           ,''
           ,null
           ,@TipoAsegurable)

set @ParentReporte = @@IDENTITY

set @AsegurableId = @@IDENTITY

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol1
           ,1
           ,@ParentReporte)

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol2
           ,1
           ,@ParentReporte)


INSERT INTO [security].[Asegurables]
           ([Nombre]
           ,[Ruta]
           ,[ParentId]
           ,[TipoAsegurableId])
     VALUES
           ('Listar Usuario'
           ,'Proyecto.AplicacionEscritorio.Vistas.frmListarUsuarios'
           ,@ParentReporte
           ,@TipoAsegurable)

set @AsegurableId = @@IDENTITY

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol1
           ,1
           ,@AsegurableId)

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol2
           ,1
           ,@AsegurableId)


	INSERT INTO [security].[Asegurables]
           ([Nombre]
           ,[Ruta]
           ,[ParentId]
           ,[TipoAsegurableId])
     VALUES
           ('Listar Asegurables'
           ,'Proyecto.AplicacionEscritorio.Vistas.frmListarAsegurables'
           ,@ParentReporte
           ,@TipoAsegurable)

		   set @AsegurableId = @@IDENTITY

INSERT INTO [security].[ControlAcceso]
           ([RolId]
           ,[TipoPermisoId]
           ,[AsegurableId])
     VALUES
           (@Rol1
           ,1
           ,@AsegurableId)




